# Task: Implement a class named 'RangeList'
# A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
# NOTE: Feel free to add any extra member variables/functions you like.
class RangeList
  attr_accessor :ranges, :integer_array

  def initialize
    @ranges = []
    @integer_array = []
  end

  # add range to range list
  def add(range)
    # TODO: implement add
    first = range.first
    last = range.last
    if first > last
      warn "Add Range is invalid"
    else
      add_range = first...last
      @integer_array = @integer_array.concat(add_range.to_a).uniq.sort
      prev = @integer_array[0]
      @ranges = @integer_array.slice_before { |e|
        prev, prev2 = e, prev
        prev2 + 1 != e
      }.map { |b, *, c| c ? (b..c) : b }
    end
  end

  # remove range from range list
  def remove(range)
    # TODO: implement remove
    first = range.first
    last = range.last
    if first > last
      warn "Remove Range is invalid"
    else
      remove_range = first...last
      @integer_array = @integer_array - remove_range.to_a
      prev = @integer_array[0]
      @ranges = @integer_array.slice_before { |e|
        prev, prev2 = e, prev
        prev2 + 1 != e
      }.map { |b, *, c| c ? (b..c) : b }
    end
  end

  # print range list
  def print
    # TODO: implement print
    Kernel::print @ranges.map { |range| "[#{range.first}, #{range.last + 1})" }.join(" ")
  end
end

rl = RangeList.new
rl.add([1, 5])
rl.print
#// Should display: [1, 5)
rl.add([10, 20])
rl.print
#// Should display: [1, 5) [10, 20)

rl.add([20, 20])
rl.print
#// Should display: [1, 5) [10, 20)

rl.add([20, 21])
rl.print
#// Should display: [1, 5) [10, 21)

rl.add([2, 4])
rl.print
#// Should display: [1, 5) [10, 21)

rl.add([3, 8])
rl.print
#// Should display: [1, 8) [10, 21)

rl.remove([10, 10])
rl.print
#// Should display: [1, 8) [10, 21)

rl.remove([10, 11])
rl.print
#// Should display: [1, 8) [11, 21)

rl.remove([15, 17])
rl.print
#// Should display: [1, 8) [11, 15) [17, 21)

rl.remove([3, 19])
rl.print
#// Should display: [1, 3) [19, 21)
