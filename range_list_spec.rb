require 'pry'
require './range_list'

describe RangeList do
  context "When testing the RangeList class" do

    it "test range list add, remove, print method" do
      rl = RangeList.new

      rl.add([1, 5])
      # Should display: [1, 5)
      expect do
        rl.print
      end.to output('[1, 5)').to_stdout

      rl.add([10, 20])
      # Should display: [1, 5) [10, 20)
      expect do
        rl.print
      end.to output('[1, 5) [10, 20)').to_stdout

      rl.add([20, 20])
      # Should display: [1, 5) [10, 20)
      expect do
        rl.print
      end.to output('[1, 5) [10, 20)').to_stdout

      rl.add([20, 21])
      # Should display: [1, 5) [10, 21)
      expect do
        rl.print
      end.to output('[1, 5) [10, 21)').to_stdout

      rl.add([2, 4])
      # Should display: [1, 5) [10, 21)
      expect do
        rl.print
      end.to output('[1, 5) [10, 21)').to_stdout

      rl.add([3, 8])
      # Should display: [1, 8) [10, 21)
      expect do
        rl.print
      end.to output('[1, 8) [10, 21)').to_stdout

      rl.remove([10, 10])
      # Should display: [1, 8) [10, 21)
      expect do
        rl.print
      end.to output('[1, 8) [10, 21)').to_stdout

      rl.remove([10, 11])
      # Should display: [1, 8) [11, 21)
      expect do
        rl.print
      end.to output('[1, 8) [11, 21)').to_stdout

      rl.remove([15, 17])
      # Should display: [1, 8) [11, 15) [17, 21)
      expect do
        rl.print
      end.to output('[1, 8) [11, 15) [17, 21)').to_stdout

      rl.remove([3, 19])
      # Should display: [1, 3) [19, 21)
      expect do
        rl.print
      end.to output('[1, 3) [19, 21)').to_stdout
    end

  end
end
